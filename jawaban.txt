-------------------------------------------------nomor 1------------------------------------------------------------------
    1. // membuat database
            create database myshop; 
    2. //memilih database untuk dikerjakan
            use database myshop; 

--------------------------------------------------nomor 2-----------------------------------------------------------------
    1. //mengisi database dengan tabel categories
            MariaDB [myshop]> create table categories(
                -> id int(20) auto_increment,
                -> nama varchar(255),
                -> primary key(id)
                -> ); 

    2. //menampilan isi tabel categories beserta tipe data yang akan disimpan
            describe categories;
            +-------+--------------+------+-----+---------+----------------+
            | Field | Type         | Null | Key | Default | Extra          |
            +-------+--------------+------+-----+---------+----------------+
            | id    | int(20)      | NO   | PRI | NULL    | auto_increment |
            | nama  | varchar(255) | YES  |     | NULL    |                |
            +-------+--------------+------+-----+---------+----------------+
            2 rows in set (0.022 sec)

    3. //mengisi database dengan tabel items
            MariaDB [myshop]> create table items(
                -> id int(20) auto_increment,
                -> name varchar(255),
                -> description varchar(255),
                -> price integer(20),
                -> stock int(8),
                -> categories_id int(20),
                -> primary key(id),
                -> foreign key(categories_id) references categories(id)
                -> );
            Query OK, 0 rows affected (0.158 sec)

    4.  // menampilkan table items berserta tipe data yang akan disimpan. categories_id akan terhubung dengan id pada categories
            MariaDB [myshop]> describe items;
            +---------------+--------------+------+-----+---------+----------------+
            | Field         | Type         | Null | Key | Default | Extra          |
            +---------------+--------------+------+-----+---------+----------------+
            | id            | int(20)      | NO   | PRI | NULL    | auto_increment |
            | name          | varchar(255) | YES  |     | NULL    |                |
            | description   | varchar(255) | YES  |     | NULL    |                |
            | price         | int(20)      | YES  |     | NULL    |                |
            | stock         | int(8)       | YES  |     | NULL    |                |
            | categories_id | int(20)      | YES  | MUL | NULL    |                |
            +---------------+--------------+------+-----+---------+----------------+
            6 rows in set (0.026 sec)

    5. ////mengisi database dengan tabel users
            MariaDB [myshop]> create table users(
                -> id int(8) auto_increment,
                -> name varchar(255),
                -> email varchar(255),
                -> password varchar(255),
                -> primary key(id)
                -> );
            Query OK, 0 rows affected (0.482 sec)

    6. // menampilkan table items berserta tipe data yang akan disimpan. 
            MariaDB [myshop]> describe users
                -> ;
            +----------+--------------+------+-----+---------+----------------+
            | Field    | Type         | Null | Key | Default | Extra          |
            +----------+--------------+------+-----+---------+----------------+
            | id       | int(8)       | NO   | PRI | NULL    | auto_increment |
            | name     | varchar(255) | YES  |     | NULL    |                |
            | email    | varchar(255) | YES  |     | NULL    |                |
            | password | varchar(255) | YES  |     | NULL    |                |
            +----------+--------------+------+-----+---------+----------------+
            4 rows in set (0.026 sec)


---------------------------------------------------------nomor 3--------------------------------------------------------
    1.// memasukan data categories
            MariaDB [myshop]> insert into categories(nama) values ("gadget"), ("cloth"),("men"),("women"),("branded");
            Query OK, 5 rows affected (0.052 sec)
            Records: 5  Duplicates: 0  Warnings: 0

    2.//menampilkan data yang masuk ke tabel categories
            MariaDB [myshop]> select * from categories
            -> ;
            +----+---------+
            | id | nama    |
            +----+---------+
            |  1 | gadget  |
            |  2 | cloth   |
            |  3 | men     |
            |  4 | women   |
            |  5 | branded |
            +----+---------+
            5 rows in set (0.001 sec)

    3.//memasukan nama ke tabel users
            MariaDB [myshop]>  insert into users(name, email, password) values
                -> ("John Doe","john@doe.com","john123"),
                -> ("Jane Doe","jane@doe.com","jenita123");
            Query OK, 2 rows affected (0.113 sec)
            Records: 2  Duplicates: 0  Warnings: 0

    4.//menampilkan data yang sudah masuk ke tabel users
            MariaDB [myshop]> select * from users
                -> ;
            +----+----------+--------------+-----------+
            | id | name     | email        | password  |
            +----+----------+--------------+-----------+
            |  1 | John Doe | john@doe.com | john123   |
            |  2 | Jane Doe | jane@doe.com | jenita123 |
            +----+----------+--------------+-----------+
            2 rows in set (0.000 sec)

    5. //memasukan data ke tabel items
            MariaDB [myshop]> insert into items(name,description,price,stock,categories_id) values
                -> ("sumsang b50", "hape murah merakyat",400000,100,1),
                -> ("unikloh", "cocok buat anak muda BPJS",50000,50,2),
                -> ("EMOH Smartwatch", "jam tangan anak orang kaya", 700000,10,1);
            Query OK, 3 rows affected (0.065 sec)
            Records: 3  Duplicates: 0  Warnings: 0
        
    6. //menampilkan data yang sudah dimasukan ke tabel items
            MariaDB [myshop]> select * from items;
            +----+-----------------+----------------------------+--------+-------+---------------+
            | id | name            | description                | price  | stock | categories_id |
            +----+-----------------+----------------------------+--------+-------+---------------+
            |  1 | sumsang b50     | hape murah merakyat        | 400000 |   100 |             1 |
            |  2 | unikloh         | cocok buat anak muda BPJS  |  50000 |    50 |             2 |
            |  3 | EMOH Smartwatch | jam tangan anak orang kaya | 700000 |    10 |             1 |
            +----+-----------------+----------------------------+--------+-------+---------------+
            3 rows in set (0.002 sec)


---------------------------------------------------------nomor 4--------------------------------------------------------    
    1. //menampilkan nama dan email pada tabel users
            MariaDB [myshop]> select name,email from users
                -> ;
            +----------+--------------+
            | name     | email        |
            +----------+--------------+
            | John Doe | john@doe.com |
            | Jane Doe | jane@doe.com |
            +----------+--------------+
            2 rows in set (0.012 sec)`

    2. //menampilkan barang yang hargannya diatas 100000
            MariaDB [myshop]> select * from items where price>=100000;
            +----+-----------------+----------------------------+--------+-------+---------------+
            | id | name            | description                | price  | stock | categories_id |
            +----+-----------------+----------------------------+--------+-------+---------------+
            |  1 | sumsang b50     | hape murah merakyat        | 400000 |   100 |             1 |
            |  3 | EMOH Smartwatch | jam tangan anak orang kaya | 700000 |    10 |             1 |
            +----+-----------------+----------------------------+--------+-------+---------------+
            2 rows in set (0.001 sec)
    
    3. //menampilkan item dari tabel items dengan fungsi like
            MariaDB [myshop]> select * from items where name like 'sums%';
            +----+-------------+---------------------+--------+-------+---------------+
            | id | name        | description         | price  | stock | categories_id |
            +----+-------------+---------------------+--------+-------+---------------+
            |  1 | sumsang b50 | hape murah merakyat | 400000 |   100 |             1 |
            +----+-------------+---------------------+--------+-------+---------------+
            1 row in set (0.005 sec)

    4. //menampilkan tabel items dan categories
            MariaDB [myshop]> select items.name, items.description, items.price, items.stock, categories.nama from items inner join categories on items.categories_id = categories.id;
            +-----------------+----------------------------+--------+-------+--------+
            | name            | description                | price  | stock | nama   |
            +-----------------+----------------------------+--------+-------+--------+
            | sumsang b50     | hape murah merakyat        | 400000 |   100 | gadget |
            | unikloh         | cocok buat anak muda BPJS  |  50000 |    50 | cloth  |
            | EMOH Smartwatch | jam tangan anak orang kaya | 700000 |    10 | gadget |
            +-----------------+----------------------------+--------+-------+--------+
            3 rows in set (0.001 sec)

---------------------------------------------------------nomor 5--------------------------------------------------------    
    1. //ubah price sumsang
            MariaDB [myshop]> update items set  price = "3000000" where id = 1;
            Query OK, 1 row affected (0.039 sec)
            Rows matched: 1  Changed: 1  Warnings: 0

            MariaDB [myshop]> select * items;
            ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'items' at line 1
            MariaDB [myshop]> select * from items;
            +----+-----------------+----------------------------+---------+-------+---------------+
            | id | name            | description                | price   | stock | categories_id |
            +----+-----------------+----------------------------+---------+-------+---------------+
            |  1 | sumsang b50     | hape murah merakyat        | 3000000 |   100 |             1 |
            |  2 | unikloh         | cocok buat anak muda BPJS  |   50000 |    50 |             2 |
            |  3 | EMOH Smartwatch | jam tangan anak orang kaya |  700000 |    10 |             1 |
            +----+-----------------+----------------------------+---------+-------+---------------+
            3 rows in set (0.000 sec)


    

    


